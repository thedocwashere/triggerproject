library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity main is
  Port (clk_main : IN std_logic;
        sig_1 : IN std_logic;
        sig_2 : IN std_logic;
        res : IN std_logic;
        out_sig : OUT std_logic;
        window : OUT std_logic);
end main;

architecture Behavioral of main is
    
    --D flip-flop component declaration
    component d_flipflop PORT(d : IN std_logic;
                              clk : IN std_logic;
                              reset : IN std_logic;
                              q : OUT std_logic;
                              qnot : OUT std_logic);
    end component; 
    
    --intermediate signal declarations for flip-flops   
    signal inter_sig_1 : std_logic := '0'; 
    signal inter2_sig_1 : std_logic := '0';
    signal rising_edge_sig_1 : std_logic := '0';     
    
    signal inter_sig_2 : std_logic := '0'; 
    signal inter2_sig_2 : std_logic := '0';
    signal rising_edge_sig_2 : std_logic := '0';  
    
    --The trigger signal
    signal window_open : std_logic := '0'; 
begin

    --Combinational part of the design
    
    --Rising edge detector for signal 1 (2 FF - source : https://ee-paper.com/how-to-realize-the-asynchronous-signal-edge-detection-circuit/)
    --I used the '0' signal for reset ports because, otherwise when I push and pull reset, it continues where it is left.
    d1 : d_flipflop PORT MAP (d => sig_1, clk => clk_main, reset => '0', q => inter_sig_1, qnot => open); 
    d2 : d_flipflop PORT MAP (d => inter_sig_1 , clk => clk_main, reset => '0', q => open , qnot => inter2_sig_1);
    rising_edge_sig_1 <= inter2_sig_1 AND inter_sig_1;
    
    --Rising edge detector for signal 2 (same design as above)
    d3 : d_flipflop PORT MAP (d => sig_2, clk => clk_main, reset => '0', q => inter_sig_2, qnot => open);
    d4 : d_flipflop PORT MAP (d => inter_sig_2 , clk => clk_main, reset => '0', q => open , qnot => inter2_sig_2);
    rising_edge_sig_2 <= inter2_sig_2 AND inter_sig_2;
    
    --trigger signal to open the window
    window_open <= rising_edge_sig_1 OR rising_edge_sig_2;
    
    --Sequential part of the design
    window_process : process(clk_main)        
        variable window_on : std_logic := '0'; -- initially the window is closed
        variable period_count : integer range 0 to 400000000 := 0; --4 second time window
        variable open_type : integer range 0 to 2; --the type which defines how did the window open? 0:simultaneously high ; 1: signal_1 high ; 2: signal_2 high
        variable temp_out : std_logic := '0';
    begin
    
    if(rising_edge(clk_main)) then
        --time window design
        if(res = '1') then
            window_on := '0';
            temp_out := '0';
            period_count := 0;
        else
            if (period_count = 400000000) then --time window closes
                period_count := 0; 
                window_on := '0';                       
            elsif (window_open = '1' AND period_count = 0) then 
                
                if(rising_edge_sig_1 = '1' AND rising_edge_sig_2 = '1') then
                    open_type := 0;
                elsif (rising_edge_sig_1 = '1') then
                    open_type := 1;
                elsif (rising_edge_sig_2 = '1') then
                    open_type := 2; 
                end if;  
                  
                period_count := 1;
                window_on := '1'; 
            elsif (period_count /= 0) then
                period_count := period_count + 1;
                window_on := '1';        
--            else
--                period_count := 0; 
--                window_on := '0'; 
            end if;  
            
            --defining the output signal 
            if (window_on = '1') then
                if (open_type = 0) then
                temp_out := '1';
                elsif (open_type = 1 AND rising_edge_sig_2 = '1') then 
                temp_out := '1';
                elsif (open_type = 2 AND rising_edge_sig_1 = '1') then 
                temp_out := '1';
                else
                temp_out := temp_out;
                end if;
            else
                temp_out := '0';  
            end if;          
         end if;             
    end if;  
    
    --output assignments
    out_sig <= temp_out;
    window <= window_on;
    end process;    
    
end Behavioral;