library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity d_flipflop is
    Port (d : IN std_logic;
          clk : IN std_logic;
          reset : IN std_logic;
          q : OUT std_logic;
          qnot : OUT std_logic
          );
end d_flipflop;

architecture Behavioral of d_flipflop is

begin

    dflipflop_proc : process (clk,reset)
    begin
        if(reset = '1') then
            q <= '0';
            qnot <= '1';
        elsif(rising_edge(clk)) then
            q <= d;
            qnot <= NOT d;
        end if;
    end process dflipflop_proc;
    
end Behavioral;
