-- For simulation the period_count variable (line 55) in the main should range from 0 to 4.
-- Also the line 67 should change too to "period_count = 4".

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity main_sim is
end main_sim;

architecture Behavioral of main_sim is
    component main 
        Port (clk_main : IN std_logic;
              sig_1 : IN std_logic;
              sig_2 : IN std_logic;
              res : IN std_logic;
              out_sig : OUT std_logic;
              window : OUT std_logic);
    end component;
    
    signal clk_main : std_logic := '0';
    signal sig_1 : std_logic := '0';
    signal sig_2 : std_logic := '0';
    signal res : std_logic := '0';
    signal out_sig : std_logic;
    signal window : std_logic;
    
    constant clk_period : time := 4 ns; --clock frequency: 250 MHz
begin
    
    uut : main PORT MAP (clk_main => clk_main,
                  sig_1 => sig_1,
                  sig_2 => sig_2,
                  res => res,
                  out_sig => out_sig,
                  window => window);
                  
    clock_process :process
    begin
      clk_main <= '0';
      wait for clk_period/2;
      clk_main <= '1';
      wait for clk_period/2;
   end process;
   
   test: process
   begin	
          -- time window opened by sig_1 and closed after expected time
          -- time window opened by sig_2 and closed after expected time
          -- time window opened by sig_1 triggered by sig_2
          -- time window opened by sig_2 then closed , opened by sig_1 and triggered by sig_2
          -- time window opened by both sig_2 & sig_1
          -- time window opened by sig_2 triggered by sig_1
          sig_1 <= '1' after 101 ns,
                   '0' after 140 ns,
                   '1' after 240 ns,
                   '0' after 270 ns,
                   '1' after 300 ns,
                   '0' after 340 ns,
                   '1' after 440 ns,
                   '0' after 455 ns,
                   '1' after 500 ns,
                   '0' after 530 ns,
                   '1' after 600 ns,
                   '0' after 630 ns,
                   '1' after 710 ns,
                   '0' after 720 ns;
          sig_2 <= '1' after 180 ns,
                   '0' after 220 ns,
                   '1' after 315 ns,
                   '0' after 330 ns,
                   '1' after 400 ns,
                   '0' after 480 ns,
                   '1' after 500 ns,
                   '0' after 540 ns,
                   '1' after 630 ns,
                   '0' after 690 ns,
                   '1' after 700 ns,
                   '0' after 720 ns;          
          wait;
  end process;
end Behavioral;
